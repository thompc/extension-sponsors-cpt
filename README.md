#Sponsors Post Type
This extension creates a custom post type 'Sponsors', and creates the relevant ACF fields.

##Dependencies
For use with Multiple Post Types Extension.

###Installation
CD to your bip-extensions directory
Clone the extension to your bip-extensions directory git clone https://username@bitbucket.org/thompc/extension-sponsors-cpt
Go to any page and add a new Multiple Post Types level
Select Sponsors from the drop down menu and select as many as needed