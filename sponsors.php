<?php
/**
 * BiP Extension: Sponsors Post Type
 * Description: Create Sponsors post type and register ACF
 * @author: Chris Thompson
 * @url: https://bitbucket.org/thompc/extension-sponsors-cpt
 * @version: 1.0
 *
 */

 //Register our post type
add_action( 'init', function() {
    $args = array(
        "labels" => array( "name" => "Sponsors", "singular_name" => "Sponsor" ),
        "description" => "",
        "public" => true,
        "show_ui" => true,
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "sponsor", "with_front" => true ),
        "query_var" => true,
        'supports' => array('title', 'editor')
    );
    register_post_type( "sponsor", $args );
});

//Get ACF to search our /acf folder for our .JSON file
add_filter('acf/settings/load_json', 'load_sponsors_acf');
function load_sponsors_acf( $paths ) {
    $paths[] = dirname(__FILE__).'/acf';
    return $paths;
}

// Image size
add_image_size( 'sponsor_logo', 200, 200, false);
